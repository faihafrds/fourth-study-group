import { Form, FormGroup, Input, Button, Spinner, Card, CardImg, CardTitle } from "reactstrap"
import { useState } from "react"
import axios from "axios"

const SearchPhone = () => {
    const [searchPhones, setSearchPhones] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [phoneName, setPhoneName] = useState("")

    const handlePhoneName = (e) => {
        setPhoneName(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setIsLoading(true)
        const params = phoneName
        const url = `http://api-mobilespecs.azharimm.tk/search?query=${params}`

        axios.get(url)
            .then(res => {
                console.log(res)
                setTimeout(() => {
                    setSearchPhones(res.data.data.phones)
                    setIsLoading(false)
                }, 1000);
            })
            .catch(err => console.log(err))



        // const params = new URLSearchParams({
        //     apikey: "6d1f3e3e",
        //     s: this.state.title
        // })
        // const url = `https://www.omdbapi.com/?${params}`

        // fetch(url)
        //     .then(res => res.json())
        // .then(data => {
        //     console.log(data)
        //     setTimeout(() => {
        //         this.setState({
        //             movies: data.Search,
        //             isLoading: false
        //         })
        //     }, 1000);
        // })
        //     .catch(err => console.log(err))
    }

    return (
        <div>
            <div className="row">
                <div className="col-12">
                    <Form inline onSubmit={handleSubmit}>
                        <FormGroup>
                            <Input className="mr-2" type="text" name="title" placeholder="Your Phone Brand" onChange={handlePhoneName} />
                        </FormGroup>
                        <Button type="submit">Search</Button>
                    </Form>
                    <hr />
                </div>
            </div>
            <div className="row">
                {isLoading && <Spinner color="danger" />}
                {searchPhones && !isLoading && 
                    searchPhones.map( (item, i) => 
                        <div className="col-3" key={i}>
                            <Card body style={{ marginBottom: "20px" }}>
                                <CardImg top src={item.phone_img_url} alt="Card image cap" />
                                <hr />
                                <CardTitle tag="h6">{item.phone_name}</CardTitle>
                                {/* <Button href={`${match.url}/${brand.phone_name_slug}`} color="primary">See more</Button> */}
                            </Card>
                        </div>
                    )
                }
            </div>


        </div>
    )
}

export default SearchPhone
