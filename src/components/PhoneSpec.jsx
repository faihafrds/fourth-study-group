import { useEffect } from "react"
import { useParams } from "react-router-dom"
import { List, Spinner} from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux'
import { selectSpec } from "../redux/action";

const PhoneSpec = () => {
    let params = useParams();

    const globalState = useSelector(state => state)
    const { brand, specList, isLoading, phoneType } = globalState
    const dispatch = useDispatch()

    const url = `https://api-mobilespecs.azharimm.tk/brands/${params.brand}/${params.type}`

    useEffect(() => {  
        getData(url)
    }, [url])

    const getData = (url) => {
        dispatch(selectSpec(url))
    }

    return (
        <div>
            <h5>{brand} {phoneType}</h5>
            <div className="row">
            {isLoading && <Spinner color="primary" />}
            <List>
                {specList && specList.map((spec, i) => (
                    <li key={i}>{spec.title}:{spec && spec.specs.map((specs, i) => (
                        <ul>
                            <li key={i}>{specs.key}: {specs.val}</li>
                        </ul>
                        ))}
                    </li>
                ))}
            </List>
            </div>
        </div>
    )
}

export default PhoneSpec
