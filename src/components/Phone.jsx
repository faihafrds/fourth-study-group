import { useEffect } from 'react';
import { NavLink, Switch, Route, useRouteMatch } from 'react-router-dom';
import PhoneList from './PhoneList'
import { useSelector, useDispatch } from 'react-redux'
import { selectBrand } from '../redux/action';

const Phone = () => {
    let match = useRouteMatch()

    const isLoading = useSelector(state => state.isLoading)
    const brandList = useSelector(state => state.brandList)
    const dispatch = useDispatch()

    const url = `https://api-mobilespecs.azharimm.tk/brands`

    useEffect(()=>{
        getData(url)
    }, [url])
    
    const getData = (url) => {
        dispatch(selectBrand(url))
    }

    return (
        <div>
            <h3 className="text-center">Phone Brands</h3>
            <nav>
                <ul className="nav justify-content-center">
                    {isLoading && <p>Loading...</p>}
                    {!isLoading && brandList && brandList.map((brand, i) => (
                        <li key={i}><NavLink className="nav-link" to={`${match.url}/${brand.brand_slug}`}>{brand.brand}</NavLink></li>
                    ))}
                </ul>
            </nav>
            <hr />
            <Switch>
                <Route path={`${match.path}/:brand`}>
                <PhoneList />
                </Route>
            </Switch>
        </div>
    )
}

export default Phone
