import { useEffect } from "react"
import { useParams, useRouteMatch, Switch, Route } from "react-router-dom"
import { Card, CardTitle, CardImg, Spinner, Button} from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux'
import PhoneSpec from './PhoneSpec'
import { selectType } from "../redux/action";

const PhoneList = () => {
    let params = useParams();
    let match = useRouteMatch()

    const isLoading = useSelector(state => state.isLoading)
    const typeList = useSelector(state => state.typeList)
    const dispatch = useDispatch()

    const url = `https://api-mobilespecs.azharimm.tk/brands/${params.brand}`

    useEffect(() => {  
        getData(url)
    }, [url])

    const getData = (url) => {
        dispatch(selectType(url))
    }

    return (
        <div>
            <div className="row pt-2">
                <div className="col-8">
                    <div className="row">
                    {isLoading && <Spinner color="primary" />}
                    {!isLoading && typeList && typeList.map((brand, i) => (
                        <div className="col-4" key={i}>
                            <Card body>
                                <CardImg top src={brand.phone_img_url} alt="Card image cap" />
                                <CardTitle tag="h5">{brand.brand} {brand.phone_name}</CardTitle>
                                <Button href={`${match.url}/${brand.phone_name_slug}`} color="primary" >See more</Button>
                            </Card>
                        </div>
                    ))}
                    </div>
                </div>
                <div className="col-4 pt-3 px-4" style={{backgroundColor: "#007bff", color:"white"}}>
                    <h4>Specification</h4>
                    <Switch>
                        <Route path={`${match.path}/:type`}>
                            <PhoneSpec/>
                        </Route>
                    </Switch>
                </div>
            </div>
        </div>
    )
}

export default PhoneList
