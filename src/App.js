import Nav from './Nav'
import AppRoutes from './AppRoutes'

const App = () => {
  return (
    <>
      <Nav/>
      <div className="container">
        <h2 className="text-center pt-2">4th Task Group</h2>
        <hr/>
        <AppRoutes/>
      </div>
    </>
  );
}

export default App;
