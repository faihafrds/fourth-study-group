import { Link } from 'react-router-dom'

const Nav = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item active pl-5"><Link className="nav-link" to="/">Home</Link></li>
                        <li className="nav-item px-3"><Link className="nav-link" to="/search">Search Phone</Link></li>
                        <li className="nav-item"><Link className="nav-link" to="/list">List Phone Brand</Link></li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default Nav
