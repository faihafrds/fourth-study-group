import { types } from './action'

const globalState = {
    brandList: [],
    typeList: [],
    specList: [],
    brand: "",
    phoneType: "",
    isLoading: false
}

const rootReducer = (state=globalState, action) => {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return{
                ...state,
                isLoading: true
            }
        case types.SELECT_BRAND:
            return{
                ...state,
                brandList: action.brandList,
                isLoading: false
            }
        case types.SELECT_TYPE:
            return{
                ...state,
                typeList: action.typeList,
                isLoading: false
            }
        case types.SELECT_SPEC:
            return{
                ...state,
                specList: action.specList,
                brand: action.brand,
                phoneType: action.phoneType,
                isLoading: false
            }
        default:
            return state;
    }
}

export default rootReducer