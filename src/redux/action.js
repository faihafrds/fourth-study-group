import axios from 'axios'

export const types = {
    CHANGE_LOADING: "CHANGE_LOADING",
    SELECT_BRAND: "SELECT_BRAND",
    SELECT_TYPE: "SELECT_TYPE",
    SELECT_SPEC: "SELECT_SPEC"
}

export const selectBrand = (url) => (dispatch) => {
    dispatch({type: types.CHANGE_LOADING})
        axios.get(url)
            .then(res => {
                dispatch({type: types.SELECT_BRAND, brandList: res.data.data.brands})
            })
            .catch(err => console.log(err))
}

export const selectType = (url) => (dispatch) => {
    dispatch({type: types.CHANGE_LOADING})
        axios.get(url)
        .then(res => {
            dispatch({type: types.SELECT_TYPE, typeList: res.data.data.phones})
        })
        .catch(err => console.log(err))
}

export const selectSpec = (url) => (dispatch) => {
    dispatch({type: types.CHANGE_LOADING})
        axios.get(url)
        .then(res => {
            dispatch({
                type: types.SELECT_SPEC, 
                specList: res.data.data.specifications,
                brand: res.data.data.brand,
                phoneType: res.data.data.phone_name
            })
        })
        .catch(err => console.log(err))
}