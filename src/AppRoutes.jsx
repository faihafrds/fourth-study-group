import { Switch, Route } from 'react-router-dom'
import Phone from './components/Phone'
import SearchPhone from './components/SearchPhone'

const AppRoutes = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/"><h3 className="text-center">Home</h3></Route>
                <Route path="/search"><SearchPhone/></Route>
                <Route path="/list"><Phone/></Route>
            </Switch>
        </div>
    )
}

export default AppRoutes
